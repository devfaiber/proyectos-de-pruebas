﻿namespace bolosTablaConteo.Models
{
    public class BoardRow: IGenerateListData
    {
        public int RowNumber { get; set; }
        public bool IsStartRow { get; set; }
        public IEnumerable<BoardColumn> Columns { get; set; }
        private readonly int totalColumns;

        public int Total { get; set; }

        public BoardRow(int rowNumber, int totalColumns)
        {
            this.RowNumber = rowNumber;
            this.totalColumns = totalColumns;
        }

        public void Generate()
        {
            var columns = new List<BoardColumn>();

            for (int i = 0; i < this.totalColumns; i++)
            {
                
                bool isStart = false;
                bool isEnd = i == this.totalColumns - 1;

                var column = new BoardColumn(i+1);
                column.IsStartColumn = isStart;
                column.IsEndColumn = isEnd;


                column.Previous = i == 0 ? null : columns.Last();
                if(column.Previous != null)
                {
                    column.Previous.Next = column;
                }

                column.Generate();
                columns.Add(column);
            }

            this.Columns = columns;
        }
    }
}
