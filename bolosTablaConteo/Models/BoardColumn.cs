﻿namespace bolosTablaConteo.Models
{
    public class BoardColumn : IGenerateListData
    {
        public int ColumnNumber { get; private set; }
        public bool IsEndColumn { get; set; }
        public bool IsStartColumn { get; set; }
        public const int MAX_BOLOS = 10;
        public const string VALUE_STRIKE = "x";
        public const string VALUE_SPARE = "-";

        public bool IsStrike { get; set; } // de un solo tiro
        public bool IsSpare { get; set; } // de dos tiros

        public IEnumerable<BoardPoint> Points { get; set; }
        public int? Total { get; set; }

        public BoardColumn Previous { get; set; }
        public BoardColumn Next { get; set; }

        public BoardColumn(int columnNumber)
        {
            this.ColumnNumber = columnNumber;
        }

        public void Generate()
        {
            var points = new List<BoardPoint>();
            var ipoint = 1;
            for (int i = 0; i < 2; i++)
            {
                var point = new BoardPoint();
                point.PointNumber = i + 1;
                points.Add(point);
                ipoint++;
            }

            if (IsEndColumn) // campo extra alfinal
            {
                points.Add(new BoardPoint
                {
                    PointNumber = ipoint,
                    IsExtra = true
                });
            }

            this.Points = points;
        }

        public void SetSpare()
        {
            var pointFirst = Points.First();
            var pointLast = Points.Last();

            IsSpare = IsStrike = false;

            if (string.IsNullOrEmpty(pointFirst.PointValue) || string.IsNullOrEmpty(pointLast.PointValue))
            {
                IsSpare = false;
                return;
            }
            if (pointFirst.PointValue == VALUE_STRIKE || pointLast.PointValue != VALUE_SPARE)
            {
                IsSpare = false;
                return;
            }

            IsSpare = true;
        }

        public bool IsCompleted()
        {
            return Points.All(p => !string.IsNullOrEmpty(p.PointValue));
        }

        public void Validation()
        {

            if (IsEndColumn)
            {
                return;
            }

            var pointFirst = Points.First();
            var pointLast = Points.Last();

            if (pointFirst.PointValue?.ToLower() == VALUE_SPARE)
            {
                pointFirst.PointValue = string.Empty;
            }
            if (pointFirst.PointValue?.ToLower() == VALUE_STRIKE)
            {
                pointLast.PointValue = "0";
            }
            if(pointLast.PointValue?.ToLower() == VALUE_STRIKE)
            {
                pointLast.PointValue = string.Empty;
            }

            IsSpare = pointLast.PointValue?.ToLower() == VALUE_SPARE;
            IsStrike = pointFirst.PointValue?.ToLower() == VALUE_STRIKE;

            Console.WriteLine(IsStrike);
        }

        public static void CalculateTotal(BoardColumn column, int index=0)
        {

            if (column == null || !column.IsCompleted())
            {
                return;
            }
            if (column.IsStrike && column.Next.IsCompleted())
            {
                column.Total = 10;
                column.Total += column.Next.Total;
            }
            if (column.IsSpare && column.Next.IsCompleted())
            {
                column.Total = 10;
                column.Total += column.Next.IsStrike ? column.Next.Total : Convert.ToInt32(column.Next.Points.First().PointValue);
            }
            if(!column.IsStrike && !column.IsSpare)
            {
                column.Total = column.Points.Sum(p => Convert.ToInt32(p.PointValue));
            }

            if(index >= 3)
            {
                column.Total = 30;
                return;
            } else
            {
                if (column.IsStrike)
                {
                    index++;
                } else
                {
                    index = 0;
                }

                CalculateTotal(column.Previous, index);
            }
            
        }

        public void ValidateTotal()
        {
            if(Total > MAX_BOLOS && !IsSpare && !IsStrike)
            {
                Total = null;
                foreach (var point in Points)
                {
                    point.PointValue = string.Empty;
                }
            }
        }
    }
}
