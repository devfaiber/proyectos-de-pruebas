﻿using Microsoft.AspNetCore.Components;
using System.Text.RegularExpressions;

namespace bolosTablaConteo.Models
{
    public class BoardPoint
    {
        public int PointNumber { get; set; }
        public string? PointValue { get; set; }
        public bool IsExtra { get; set; } = false;

        public bool Disabled { get; set; } = false;

        public void ValidationOrEmpty()
        {
            string pattern = @"^([0-9x-]{1})$";
            if(string.IsNullOrEmpty(PointValue) || !Regex.IsMatch(PointValue.ToLower(), pattern))
            {
                PointValue = string.Empty;
            }
        }
    }
}
