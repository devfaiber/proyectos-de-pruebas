﻿namespace bolosTablaConteo.Models
{
    public class Board: IGenerateListData
    {
        public IEnumerable<BoardRow> Rows { get; set; }

        public readonly int totalRows;
        public readonly int totalColumns;

        public Board(int totalRows, int totalColumns)
        {
            this.totalRows = totalRows;
            this.totalColumns = totalColumns;
        }

        public void Generate()
        {
            List<BoardRow> rows = new List<BoardRow>();
            for(int i = 0; i < this.totalRows; i++)
            {
                var row = new BoardRow(i + 1, this.totalColumns);
                row.Generate();
                rows.Add(row);
            }
            Rows = rows;
        }
    }
}
